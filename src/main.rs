use anyhow::{anyhow, Result};
use clap::{ArgEnum, Parser};
use reqwest::header::HeaderMap;
use reqwest::Body;
use std::os::unix::prelude::MetadataExt;
use tokio::fs::File;
use tokio_util::io::ReaderStream;
use tracing::{info, info_span, Instrument};
use tracing_subscriber::{fmt, EnvFilter};

const READER_CAPACITY: usize = 256_000; // 256KB

/// Mode of the upload
#[derive(Clone, Debug, ArgEnum)]
enum Mode {
    /// S3 compatible
    S3,
    /// Raw HTTP POST
    Raw,
}

/// This doc string acts as a help message when the user runs '--help'
/// as do all doc strings on fields
#[derive(Debug, Parser)]
struct Opts {
    #[clap(arg_enum)]
    mode: Mode,
    bucket_url: String,
    file_name: String,
    #[clap(long)]
    secret_key: Option<String>,
    #[clap(long)]
    access_key: Option<String>,
}

enum RequestParams {
    S3(S3RequestParams),
    Raw(RawRequestParams),
}

impl RequestParams {
    fn file_name(&self) -> &str {
        match self {
            RequestParams::S3(s3) => &s3.file_name,
            RequestParams::Raw(raw) => &raw.file_name,
        }
    }
}

struct S3RequestParams {
    bucket_url: String,
    file_name: String,
    secret_key: String,
    access_key: String,
}

struct RawRequestParams {
    url: String,
    file_name: String,
}

#[tokio::main]
async fn main() -> Result<()> {
    setup_logging();

    let opts = Opts::parse();
    let req_params = to_request_params(opts)?;

    upload(req_params).await?;

    Ok(())
}

fn to_request_params(opts: Opts) -> Result<RequestParams> {
    let res = match opts.mode {
        Mode::S3 => RequestParams::S3(S3RequestParams {
            bucket_url: opts.bucket_url,
            file_name: opts.file_name,
            secret_key: opts
                .secret_key
                .ok_or_else(|| anyhow!("secret_key required in S3 mode"))?,
            access_key: opts
                .access_key
                .ok_or_else(|| anyhow!("access_key required in S3 mode"))?,
        }),
        Mode::Raw => RequestParams::Raw(RawRequestParams {
            url: opts.bucket_url,
            file_name: opts.file_name,
        }),
    };
    Ok(res)
}

async fn upload(req_params: RequestParams) -> Result<()> {
    let file_meta = std::fs::metadata(req_params.file_name())?;
    let file = File::open(req_params.file_name()).await?;

    let client = reqwest::Client::builder()
        .danger_accept_invalid_certs(true)
        .https_only(true)
        .build()?;

    // body(file) should now work but doesn't...
    let body = Body::wrap_stream(ReaderStream::with_capacity(file, READER_CAPACITY));

    let req_builder = match req_params {
        RequestParams::S3(s3) => {
            let url = &s3.bucket_url;
            let mut headers = HeaderMap::new();
            add_v4_signature_headers(url, &s3.access_key, &s3.secret_key, &mut headers);

            // ahhhh streaming won't work - unless we send sig with every chunk??
            // see https://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-streaming.html
            headers.insert(
                reqwest::header::CONTENT_LENGTH,
                reqwest::header::HeaderValue::try_from(file_meta.size()).unwrap(),
            );

            client.put(url).headers(headers).body(body)
        }
        RequestParams::Raw(raw) => {
            let url = &raw.url;
            let headers = HeaderMap::new();

            client.post(url).headers(headers).body(body)
        }
    };

    let span = info_span!("request.upload");
    let resp = req_builder.send().instrument(span).await?;

    info!("status: {}", resp.status());
    info!("{:?}", resp);

    Ok(())
}

fn add_v4_signature_headers(url: &str, access_key: &str, secret_key: &str, headers: &mut HeaderMap) {
    let datetime = chrono::Utc::now();

    // copied from aws-sign-v4 example
    headers.insert(
        "X-Amz-Date",
        datetime.format("%Y%m%dT%H%M%SZ").to_string().parse().unwrap(),
    );
    headers.insert("X-Amz-Content-Sha256", "UNSIGNED-PAYLOAD".parse().unwrap());
    headers.insert("host", "my-host".parse().unwrap());

    let s = aws_sign_v4::AwsSign::new("PUT", url, &datetime, headers, "us-east-1", access_key, secret_key);
    let signature = s.sign();

    info!("aws sig: {:#?}", signature);

    headers.insert(reqwest::header::AUTHORIZATION, signature.parse().unwrap());
}

fn setup_logging() {
    let _subscriber = fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .pretty()
        .with_thread_ids(true)
        .finish();

    tracing::subscriber::set_global_default(_subscriber).expect("setting default subscriber failed");
}
