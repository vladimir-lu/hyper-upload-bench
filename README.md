## small bench to upload files to minio using hyper


Use `./scripts/run_minio.sh` to start a minio. Then use `mc` or the web ui to create a test bucket (here `testbucket`)

Build with `cargo clippy`

Run like:

```
RUST_LOG=info cargo run -- --access-key user --secret-key password s3 https://127.0.0.1:9505/testbucket/zero-100mb /tmp/zero-100mb 
```