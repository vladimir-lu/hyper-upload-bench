#!/bin/sh -eu

# taken from https://gist.github.com/PhilipSchmid/1fd2688ace9f51ecaca2788a91fec133
# Usage: ./minio-upload my-bucket my-file.zip

bucket=$1
file=$2

host=127.0.0.1:9505
s3_key=user
s3_secret=password

resource="/${bucket}/${file}"
content_type="application/octet-stream"
date=`date -R`
_signature="PUT\n\n${content_type}\n${date}\n${resource}"
signature=`echo -en ${_signature} | openssl sha1 -hmac ${s3_secret} -binary | base64`

curl -v -k -X PUT -T "${file}" \
          -H "Host: ${host}" \
          -H "Date: ${date}" \
          -H "Content-Type: ${content_type}" \
          -H "Authorization: AWS ${s3_key}:${signature}" \
          https://${host}${resource}
