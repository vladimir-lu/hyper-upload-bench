#!/bin/sh -eu

HOSTPORT="127.0.0.1:9555"
API_HOSTPORT="127.0.0.1:9505"

export MINIO_ROOT_USER=user
export MINIO_ROOT_PASSWORD=password

err() { echo "ERR: $@"; exit 1; }

# check pre-reqs
type minio >/dev/null 2>&1 || err "minio is not on PATH"
type openssl >/dev/null 2>&1 || err "openssl is not on PATH"

# check self-signed certificates
if [ ! -d "cert" ]; then 
    # generate self-signed cert using openssl
    mkdir cert
    openssl genrsa -out cert/private.key 2048

    cat << "EOF" > cert/openssl.conf
[req]
distinguished_name = req_distinguished_name
x509_extensions = v3_req
prompt = no

[req_distinguished_name]
C = US
ST = VA
L = Somewhere
O = MyOrg
OU = MyOU
CN = MyServerName

[v3_req]
subjectAltName = @alt_names

[alt_names]
IP.1 = 127.0.0.1
DNS.1 = localhost
EOF
    openssl req -new -x509 -nodes -days 730 -key cert/private.key -out cert/public.crt -config cert/openssl.conf
fi

DATA_DIR=$(mktemp -d)
echo "I: storing data in $DATA_DIR"

# start minio
exec minio -S cert server "$DATA_DIR" --address "$API_HOSTPORT" --console-address "$HOSTPORT" | tee minio.log
