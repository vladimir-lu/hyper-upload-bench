## random research notes

* Hyper body cannot be converted to AsyncRead efficiently
  * https://github.com/seanmonstar/reqwest/issues/482
  * there's a crate https://crates.io/crates/stream-body, which uses https://github.com/routerify/async-pipe-rs
* Examples of conversions between AsyncRead <-> Stream: https://jsdw.me/posts/rust-futures-tokio/
* Some historical discussion about the Async* traits for tokio: 5* comment btw https://github.com/tokio-rs/tokio/pull/1744#issuecomment-553575438
* Async file i/o not fully supported because of *nix api limitations until you get to io_uring: https://github.com/async-rs/async-std/issues/724
* 


## Results on linux

* Ryzen 4800H CPU, 64GB RAM
* Fedora 35, kernel 5.14.14
* btrfs on /, tmpfs on /tmp

Minio is writing to /tmp too.

create a random 500mb file:

```shell
$ fallocate /tmp/rand-500mb -l 0.5GiB 
$ time shred -n 1 -v /tmp/rand-500mb
shred: /tmp/rand-500mb: pass 1/1 (random)...
shred -n 1 -v /tmp/rand-500mb  0.14s user 0.06s system 99% cpu 0.202 total
```

Since `/tmp` is mounted as tmpfs, we have 3.3GB/s throughput. Not realistic for reading on a slower disk, but good for checking pure TLS/upload overhead.


upload 500mb random file with curl client:

```shell
$ time ./scripts/upload_file.sh testbucket /tmp/rand-500mb

*   Trying 127.0.0.1:9505...
* Connected to 127.0.0.1 (127.0.0.1) port 9505 (#0)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*  CAfile: /etc/pki/tls/certs/ca-bundle.crt
*  CApath: none
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.3 (IN), TLS handshake, Encrypted Extensions (8):
* TLSv1.3 (IN), TLS handshake, Certificate (11):
* TLSv1.3 (IN), TLS handshake, CERT verify (15):
* TLSv1.3 (IN), TLS handshake, Finished (20):
* TLSv1.3 (OUT), TLS change cipher, Change cipher spec (1):
* TLSv1.3 (OUT), TLS handshake, Finished (20):
* SSL connection using TLSv1.3 / TLS_AES_128_GCM_SHA256
* ALPN, server accepted to use http/1.1
* Server certificate:
*  subject: C=US; ST=VA; L=Somewhere; O=MyOrg; OU=MyOU; CN=MyServerName
*  start date: Nov 12 21:28:33 2021 GMT
*  expire date: Nov 12 21:28:33 2023 GMT
*  issuer: C=US; ST=VA; L=Somewhere; O=MyOrg; OU=MyOU; CN=MyServerName
*  SSL certificate verify result: self signed certificate (18), continuing anyway.
> PUT /testbucket//tmp/rand-500mb HTTP/1.1
> Host: 127.0.0.1:9505
> User-Agent: curl/7.79.1
> Accept: */*
> Date: Sat, 13 Nov 2021 00:27:18 +0100
> Content-Type: application/octet-stream
> Authorization: AWS user:mPy21QpneBNLVRfjZWXwZrHTAdc=
> Content-Length: 536870912
> Expect: 100-continue
> 
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
* Mark bundle as not supporting multiuse
< HTTP/1.1 100 Continue
* We are completely uploaded and fine
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Accept-Ranges: bytes
< Content-Length: 0
< Content-Security-Policy: block-all-mixed-content
< ETag: "f81f5e257e1e3d6a26ed7c4110623521"
< Server: MinIO
< Strict-Transport-Security: max-age=31536000; includeSubDomains
< Vary: Origin
< Vary: Accept-Encoding
< X-Amz-Request-Id: 16B6F0382F7BE542
< X-Content-Type-Options: nosniff
< X-Xss-Protection: 1; mode=block
< Date: Fri, 12 Nov 2021 23:27:19 GMT
< 
* Connection #0 to host 127.0.0.1 left intact
./scripts/upload_file.sh testbucket /tmp/rand-500mb  0.19s user 0.28s system 43% cpu 1.067 total
```

That's about 503 MBps

Using the hyper client:

```shell
$ cargo build --release
$ time RUST_LOG=info ./target/release/hyper-upload-bench --access-key user --secret-key password https://127.0.0.1:9505/testbucket/rand-500mb /tmp/rand-500mb
Sig: "AWS4-HMAC-SHA256 Credential=user/20211112/us-east-1/s3/aws4_request,SignedHeaders=host;x-amz-content-sha256;x-amz-date,Signature=ee4335c290cd4537116004ad114d960f233aee8b5b5033b50a3bad12e7e030f7"
  2021-11-12T23:37:52.621504Z  INFO hyper_upload_bench: status: 200 OK
    at src/main.rs:70 on  ThreadId(1)

Response { url: Url { scheme: "https", cannot_be_a_base: false, username: "", password: None, host: Some(Ipv4(127.0.0.1)), port: Some(9505), path: "/testbucket/rand-500mb", query: None, fragment: None }, status: 200, headers: {"accept-ranges": "bytes", "content-length": "0", "content-security-policy": "block-all-mixed-content", "etag": "\"f81f5e257e1e3d6a26ed7c4110623521\"", "server": "MinIO", "strict-transport-security": "max-age=31536000; includeSubDomains", "vary": "Origin", "vary": "Accept-Encoding", "x-amz-request-id": "16B6F0CB86C393DE", "x-content-type-options": "nosniff", "x-xss-protection": "1; mode=block", "date": "Fri, 12 Nov 2021 23:37:52 GMT"} }
RUST_LOG=info ./target/release/hyper-upload-bench --access-key user  password  0.49s user 0.67s system 104% cpu 1.110 total

```

483 MBps. That's 96% of the speed of curl aka good enough OOTB.